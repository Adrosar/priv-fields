const alias: string = "[[Private]]";
var wm: WeakMap<object, any> | undefined;

if (typeof WeakMap === "function") {
    wm = new WeakMap();
}

function _defProp(ref: any, alias: string, value: any): void {
    if (Object && Object.defineProperty) {
        Object.defineProperty(ref, alias, {
            configurable: true,
            enumerable: false,
            writable: false,
            value: value
        });
    } else {
        (<any>ref)[alias] = value;
    }
}

function _set(ref: any, value: any): void {
    if (wm) {
        wm.set(ref, value);
    } else {
        _defProp(ref, alias, value);
    }
}

function _get(ref: any): any {
    if (wm && wm.get) {
        return wm.get(ref);
    }

    return (<any>ref)[alias];
}

function _del(ref: any): void {
    if (wm && wm.delete) {
        wm.delete(ref);
    } else {
        _defProp(ref, alias, undefined);
    }
}

export class Private<K extends object, V extends object> {

    public initValue(ref: K, value: V) {
        _set(ref, value);
    }

    public createAccess() {
        return (ref: K): V => {
            const priv: V | undefined = _get(ref);

            if (typeof priv === "undefined") {
                throw 'tQBv9s';
            }

            return priv;
        }
    }

    public getValue(ref: K): V | undefined {
        return _get(ref);
    }

    public clearValue(ref: K): void {
        _del(ref);
    }
}