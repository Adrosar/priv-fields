import { Private } from "../source";

if (typeof Private !== "function") {
    throw 'bqMDW7';
}

type Priv = {
    name: string;
}

const priv = new Private<Foo, Priv>();
const __ = priv.createAccess();

if (typeof priv !== "object") {
    throw 'WrKwpn';
}

if (typeof priv.createAccess !== "function") {
    throw 'yQQdqw';
}

if (typeof priv.initValue !== "function") {
    throw 'EQtSZ7';
}

if (typeof priv.getValue !== "function") {
    throw 'aQenkg';
}

if (typeof priv.clearValue !== "function") {
    throw 'ZxVR4M';
}

if (typeof __ !== "function") {
    throw 'IP7s6X';
}

class Foo {
    constructor(name: string) {
        priv.initValue(this, {
            name: name
        });
    }

    public getName(): string {
        return __(this).name;
    }

    public destroy(): void {
        priv.clearValue(this);
    }
}

const foo: Foo = new Foo("Foo Boo");

if (foo.getName() !== "Foo Boo") {
    throw 'N97x8e';
}

const value1: Priv | undefined = priv.getValue(foo);
if (typeof value1 !== 'object') {
    throw 'u2Ns5j';
}

foo.destroy();

const value2: Priv | undefined = priv.getValue(foo);
if (typeof value2 !== 'undefined') {
    throw 'OUsVub';
}
