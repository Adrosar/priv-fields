// Moduły:
const path = require('path');

// Funkcje:
function projectResolve(_name) {
    return path.resolve(__dirname, '..', _name);
}

function getProjectEnv() {
    const env = process.env['ENV'];

    if (typeof env === 'string') {
        return env;
    }

    throw 'AF56cd';
}

// Eksport:
module.exports = {
    projectResolve: projectResolve,
    getProjectEnv: getProjectEnv
};