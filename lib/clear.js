#!/usr/bin/env node

const fse = require('fs-extra');
const helpers = require('./helpers');

function clear(_path) {
    fse.emptyDirSync(helpers.projectResolve(_path));
}

function remove(_path) {
    fse.removeSync(helpers.projectResolve(_path));
}

function main() {
    const argv = process.argv || [];
    var matching = 0;

    for (let i = 0; i < argv.length; i++) {
        const item = argv[i];

        if (item.indexOf('--all') === 0) {
            matching++;
            clear('build');
            clear('dist');
            remove('.rpt2_cache');
            remove('.temp');
            remove('typedoc');
        }

        if (item.indexOf('--temp') === 0) {
            matching++;
            clear('.temp');;
        }

        if (item.indexOf('--dist') === 0) {
            matching++;
            clear('dist');
        }

        if (item.indexOf('--cache') === 0) {
            matching++;
            remove('.rpt2_cache');
        }

        if (item.indexOf('--typedoc') === 0) {
            matching++;
            remove('typedoc');
        }
    }

    if (matching === 0) {
        clear('build');
        clear('dist');
        remove('.rpt2_cache');
        remove('.temp');
    }
}

if (true) {
    main();
}