"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const alias = "[[Private]]";
var wm;
if (typeof WeakMap === "function") {
    wm = new WeakMap();
}
function _defProp(ref, alias, value) {
    if (Object && Object.defineProperty) {
        Object.defineProperty(ref, alias, {
            configurable: true,
            enumerable: false,
            writable: false,
            value: value
        });
    }
    else {
        ref[alias] = value;
    }
}
function _set(ref, value) {
    if (wm) {
        wm.set(ref, value);
    }
    else {
        _defProp(ref, alias, value);
    }
}
function _get(ref) {
    if (wm && wm.get) {
        return wm.get(ref);
    }
    return ref[alias];
}
function _del(ref) {
    if (wm && wm.delete) {
        wm.delete(ref);
    }
    else {
        _defProp(ref, alias, undefined);
    }
}
class Private {
    initValue(ref, value) {
        _set(ref, value);
    }
    createAccess() {
        return (ref) => {
            const priv = _get(ref);
            if (typeof priv === "undefined") {
                throw 'tQBv9s';
            }
            return priv;
        };
    }
    getValue(ref) {
        return _get(ref);
    }
    clearValue(ref) {
        _del(ref);
    }
}
exports.Private = Private;
