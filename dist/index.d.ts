export declare class Private<K extends object, V extends object> {
    initValue(ref: K, value: V): void;
    createAccess(): (ref: K) => V;
    getValue(ref: K): V | undefined;
    clearValue(ref: K): void;
}
