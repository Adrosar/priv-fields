
# Private Fields

Private fields in JavaScript and TypeScript.

## Installation

From  [npmjs.com](https://www.npmjs.com) _(recommended)_ - choose one of the options:

 - `npm install priv-fields`
 - `yarn add priv-fields`

From [Adrosar/priv-fields](https://bitbucket.org/Adrosar/priv-fields) - downloads the entire contents of the repository:
```
npm install bitbucket:Adrosar/priv-fields
```

## How to use

```typescript
import { Private } from  "priv-fields";

type Priv = {
    name: string;
}

const priv = new Private<Foo, Priv>();
const _ = priv.createAccess();

class Foo {
    constructor(name: string) {
        priv.initValue(this, {
            name: name
        });
    }

    public getName(): string {
        return _(this).name;
    }
}

const foo: Foo = new Foo("Foo");
console.log(foo.getName()); //> "Foo"
```

## Development

### Running the tests:

For Node.js:
```
npm run node:test
```

For browser:

 1. Run in terminal `npm run rollup:test`
 2. Run in terminal `npm run server`
 3. Open link http://127.0.0.1:61088/test.rollup.html
 4. Open DevTools Console _(**F12** in Google Chrome / Windows)_
 5. Use **F5** to refresh the page
 6. If there is no exception, then the test passed successfully


## Versioning

I use the versioning system [SemVer](http://semver.org/) _(2.0.0)_

## Author

* **Adrian Gargula**

## License

This project is licensed under the ISC License - see the [wiki/ISC_license](https://en.wikipedia.org/wiki/ISC_license)

## Other

This project is based on [Adrosar/ts-startek-kit](https://bitbucket.org/Adrosar/ts-startek-kit) [_(version 1.5.0)_](https://bitbucket.org/Adrosar/ts-startek-kit/src/1.5.0/)